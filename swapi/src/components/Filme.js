import React, { Component } from 'react'
import axios from 'axios'
import './Filme.css'


export default class Filme extends Component {


    constructor(props) {
        super(props);

        this.state = {
            title: "",

            films: [
                {
                    release_date: '',
                    director: '',
                    opening_crawl: '',
                    episode_id: '',
                }
            ],
            planets:'',
            characters:'',
            vehicles: '',
            species: ''
        }
    }


    componentDidMount() {


        axios.get(`https://swapi.co/api/films/?search=${this.props.title}`).then(res => {
            const films = res.data.results[0];
            this.setState({ films });

            this.setState({ director: films.director })
            this.setState({ release_date: films.release_date })
            this.setState({ opening_crawl: films.opening_crawl })
            this.setState({ episode_id: films.episode_id })
            this.setState({ planets: films.planets })

            //console.log(this.state.planets)



            /*this.getPlanetas();
            this.getPersonagens();
            this.getVeiculos();
            this.getEspecies();*/

        });


    }
    getPlanetas=()=> {

        Promise.all(this.state.films.planets.map(element => axios.get(element)))
            .then(response => {
                console.log('--planetas--');

                var list = document.createElement('ul');
               
                for (var i = 0; i < response.length; i++) {
                     //cria um item de lista:
                    var item = document.createElement('li');

                    // Set cada item:
                    item.appendChild(document.createTextNode(response[i].data.name));

                    // adiciona-os a lista:
                    list.appendChild(item);
                }
                console.log(list);
                
               
                
                return list
               
               
            });           
            
        }
            

    getPersonagens=()=> {
        Promise.all(this.state.films.characters.map(element => axios.get(element)))
        .then(response => {
            console.log('--personagens--');

            var list = document.createElement('ul');
           
            for (var i = 0; i < response.length; i++) {
                //cria um item de lista:
                var item = document.createElement('li');

                // Set cada item:
                item.appendChild(document.createTextNode(response[i].data.name));

                // adiciona-os a lista:
                list.appendChild(item);
            }
            console.log(list);
            
           
            
            return list
           
           
        });           
    }
    getVeiculos=()=> {
        Promise.all(this.state.films.vehicles.map(element => axios.get(element)))
        .then(response => {
            console.log('--veiculos--');

            var list = document.createElement('ul');
           
            for (var i = 0; i < response.length; i++) {
                //cria um item de lista:
                var item = document.createElement('li');

                // Set cada item:
                item.appendChild(document.createTextNode(response[i].data.name));

                // adiciona-os a lista:
                list.appendChild(item);
            }
            console.log(list);
            
           
            
            return list
           
           
        }); 
    }
    getEspecies=()=> {

        Promise.all(this.state.films.species.map(element => axios.get(element)))
        .then(response => {
            console.log('--especies--');

            var list = document.createElement('ul');
           
            for (var i = 0; i < response.length; i++) {
                //cria um item de lista:
                var item = document.createElement('li');

                // Set cada item:
                item.appendChild(document.createTextNode(response[i].data.name));

                // adiciona-os a lista:
                list.appendChild(item);
            }
            console.log(list);
            
           
            
            return list
           
           
        }); 
    }



    render() {

        return (


            <div className="filme">
                <h2>Filme: {this.props.title}</h2>
                <h2>Diretor: {this.state.director}</h2>
                <h2>Lançamento: {this.state.release_date}</h2>
                <h2>Sinopse: {this.state.opening_crawl}</h2>

                <button className="planetasButton" value="Planeta" onClick={this.getPlanetas}> Planetas </button>
                <button className="personagensButton" value="Personagem"  onClick={this.getPersonagens}> Personagens </button>
                <button className="veiculosButton" value="Veiculos" onClick={this.getVeiculos}> Veículos </button>
                <button className="especiesButton" value="Especie"  onClick={this.getEspecies}> Espécies </button>
                <div id="divResultado">
                    
                
                </div>
            </div>
        )
    }




}
