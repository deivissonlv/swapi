import React from 'react';

import './App.css';
import Filme from './components/Filme';






function App() {
  return (
    <div className="App">
      <div className="Linha">
      <img src="https://m.media-amazon.com/images/M/MV5BYTRhNjcwNWQtMGJmMi00NmQyLWE2YzItODVmMTdjNWI0ZDA2XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SY999_SX666_AL_.jpg" alt="the phantom menace" width="360px" height="600px;"></img>
      
        <Filme title="The Phantom Menace"   ></Filme>
      </div>
      <div className="Linha">
      <img src="https://images-na.ssl-images-amazon.com/images/I/51BGV8AJ4RL._SY445_.jpg" ></img>
        <Filme title="Attack of the Clones"  ></Filme>
      </div>
      <div className="Linha">
      <img src="https://images-na.ssl-images-amazon.com/images/I/51RHXMVH9YL._SY445_.jpg"></img>
        <Filme title="Revenge of the Sith" ></Filme>
      </div>
      <div className="Linha">
      <img src ="https://images-na.ssl-images-amazon.com/images/I/51eZHIXDM1L._SX303_BO1,204,203,200_.jpg"></img>
        <Filme title="A New Hope"></Filme>
      </div>
      <div className="Linha">
        <img src="https://images-na.ssl-images-amazon.com/images/I/51QvoX064kL._SX303_BO1,204,203,200_.jpg"></img>
        <Filme title="The Empire Strikes Back"  ></Filme>
      </div>
      <div className="Linha">
      <img src ="https://images-na.ssl-images-amazon.com/images/I/51UB-i-tjdL._SX302_BO1,204,203,200_.jpg"></img>
        <Filme title="Return of the Jedi"  ></Filme>
      </div>
      <div className="Linha">
      <img src="https://images-na.ssl-images-amazon.com/images/I/814X0lSCJQL._SY445_.jpg"></img>
        <Filme title="The Force Awakens"></Filme>
      </div>
    </div>
  );
}

export default App;
